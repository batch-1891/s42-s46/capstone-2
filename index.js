const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')




const app = express()

const port = 4000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use("/users", userRoutes)
app.use("/product", productRoutes)





mongoose.connect("mongodb+srv://admin123:admin123@zuitt-bootcamp.cuxdw.mongodb.net/Capstone-2-S42-S46?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection

db.on('error', () => console.error.bind(console, 'error'))
db.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})