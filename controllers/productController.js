const Product = require('../models/Product');
const User = require('../models/User');


module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newProduct.save().then((product, error) => {

                if(error) {
                    return false
                } else {

                    return product
                }
            })
        }
        
    });    
}

module.exports.getAllProduct = async (data) => {

        return Product.find({}).then(result => {

            return result
        })
} 



module.exports.getSingleProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result
    })

}

module.exports.updateProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {

            return "You are not an admin"

        } else {

            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newProduct.save().then((product, error) => {

                if(error) {
                    return false
                } else {

                    return product
                }
            })
        }   
    })   
}

module.exports.archiveProduct = (data) => {

    return Product.findById(data.productId).then((result, err) => {

        if(data.payload === true) {

            result.isActive = false;

            return result.save().then((archivedProduct, err) => {

                if(err) {

                    return false;

                } else {


                    return archivedProduct;
                }
            })

        } else {

            return false
        }
    })
}
