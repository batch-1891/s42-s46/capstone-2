const User = require('../models/User');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return user
		}

	})
}

module.exports.registerAdmin = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10), 
		isAdmin: true
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return user
		}

	})
}

module.exports.authentication = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
}



module.exports.checkout = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == true) {
            return "Admin is not allowed"
        } else {
            let newOrder = new Order({
                totalAmount: reqBody.totalAmount
            })

            return newOrder.save().then((amount, error) => {

                if(error) {
                    return false
                } else {

                    return amount
                }
            })
        }
        
    });    
}


module.exports.getAllOrder = async (data) => {

	if (data.isAdmin) {
		return Order.find({}).then(result => {

			return result
		})
	} else {

		return false
	}
}

module.exports.getAllUserOrder = async (data) => {

	if (data.isAdmin == false) {
		return Order.find({}).then(result => {

			return result
		})
	} else {

		return false
	}
}


