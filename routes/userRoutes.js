const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");



router.post("/registerUser", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/authentication", (req, res) => {
	userController.authentication(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.checkout(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.get("/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getAllOrder(userData).then(resultFromController => res.send(resultFromController))
});

router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getAllUserOrder(userData).then(resultFromController => res.send(resultFromController))
});



module.exports = router;