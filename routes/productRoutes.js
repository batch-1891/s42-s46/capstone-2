const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


router.post("/addProduct", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {

    productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {

    productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))

});

router.put("/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.updateProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.put('/:productId/archive', auth.verify, (req, res) => {

    const data = {
        productId : req.params.productId,
        payload : auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});





module.exports = router;